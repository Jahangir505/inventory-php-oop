<?php

namespace Galaxy\Brands;

use Galaxy\Database;
use Galaxy\Utility\Helper;

$database = new Database();

class Brand
{

    public $title = null;
    public $description = null;
    public $image_url = null;

    public function getAllBrand()
    {
        return $this->database->showData("brands", 5);
    }

    public function getBrandById($id)
    {
        return $this->database->getById($id, "brands");
    }
}
