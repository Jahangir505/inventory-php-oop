<?php

namespace Galaxy\Categories;

use Galaxy\Database;

$database = new Database();

class Category
{
    public $title = null;
    public $description = null;
    public $image_url = null;

    public function getAllCategory()
    {
        return $this->database->showData('categories', 5);
    }


    public function categoryById($id)
    {
        return $this->database->getById($id, 'categories');
    }
}
