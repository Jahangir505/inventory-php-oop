<?php

namespace Galaxy;

use Galaxy\Utility\Helper;
use PDOException;

class Database
{
    private $host = "localhost";
    private $user = "root";
    private $db = "inventory_php";
    private $pass = "";
    public $conn;

    public function __construct()
    {
        $this->conn = new \PDO("mysql:host=" . $this->host . ";dbname=" . $this->db, $this->user, $this->pass);
    }

    public function showData($table, $limit)
    {
        $sql = "SELECT * FROM $table ORDER BY id LIMIT $limit";
        $q = $this->conn->query($sql) or die("failed!");

        while ($r = $q->fetch(\PDO::FETCH_ASSOC)) {
            $data[] = $r;
        }
        return $data;
    }

    public function getById($id, $table)
    {
        $sql = "SELECT * FROM $table WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id' => $id));
        $data = $q->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }


    public function deleteData($id, $table)
    {
        $sql = "DELETE FROM $table WHERE $table.`id` = $id";

        // print_r($sql);
        // die;

        $smtp = $this->conn->prepare($sql);

        $result = $smtp->execute();

        return $result;
    }

    // Brand Store or Update

    public function brandStoreOrUpdate($id, $data)
    {

        try {
            if ($id > 0) {
                $sql = "UPDATE `brands` SET `title` = :title, `description` = :description, `image_url` = :image_url WHERE `brands`.`id` = :id";
            } else {
                $sql = "INSERT INTO `brands` (`id`, `title`, `description`, `image_url`, `created_at`, `updated_at`) VALUES (NULL, :title, :description, :image_url, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
            }


            $stmt = $this->conn->prepare($sql);
            $result = $stmt->execute($data);
            if ($result) {
                echo "Data Submit Successfully!";
                Helper::redirect('http://php-oop.test/Inventory-PHP/backend/brands/brand.php');
            } else {
                echo "Data Submit Faild!";
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    // Category Store or Update

    public function categoryStoreOrUpdate($id, $data)
    {

        try {
            if ($id > 0) {
                $sql = "UPDATE `categories` SET `title` = :title, `description` = :description, `image_url` = :image_url WHERE `categories`.`id` = :id";
            } else {
                $sql = "INSERT INTO `categories` (`id`, `title`, `description`, `image_url`, `created_at`, `updated_at`) VALUES (NULL, :title, :description, :image_url, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
            }


            $stmt = $this->conn->prepare($sql);
            $result = $stmt->execute($data);
            if ($result) {
                echo "Data Submit Successfully!";
                Helper::redirect('http://php-oop.test/Inventory-PHP/backend/categories/category.php');
            } else {
                echo "Data Submit Faild!";
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
