<?php

namespace Galaxy\Utility;

class Helper extends Utility
{

    public function redirect($url)
    {
        header('location:' . $url);
    }


    public static function loadPartial($partialname, $path)
    {
        global $webroot_admin;
        include_once($path . "/partials/" . $partialname);
    }
}
