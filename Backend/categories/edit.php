<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/config.php');

use Galaxy\Database;

$id = $_GET['id'];

$category = new Database();

$data = $category->getById($id, 'categories');

// dd($data_list);
?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/header.php'); ?>
<!-- include file sidebar -->
<div class="iq-sidebar sidebar-default" id="sidebar">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/sidebar.php'); ?>
</div>
<!-- include file topnavbar -->
<div class="iq-top-navbar" id="topbar">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/topbar.php'); ?>
</div>

<div class="content-page">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <div class="table-responsive rounded mb-3">
                    <form action="store.php" method="POST" enctype="multipart/form-data">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="categoryForm">category Update</h5>

                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12 col-lg-12 col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <input type="hidden" name="id" value="<?= $data['id']; ?>">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>category Name *</label>
                                                                <input type="text" class="form-control" name="title" placeholder="Enter category Name" value="<?= $data['title']; ?>" />
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Description </label>
                                                                <textarea type="text" class="form-control" name="description" placeholder="Enter Code"> <?= $data['description']; ?></textarea>
                                                                <div class="help-block with-errors"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="hidden" value="<?= $data['image_url']; ?>" name="picture_old">
                                                            <img src="<?= $webroot . '/storage/uploads/' . $data['image_url']; ?>" alt="" height="30">
                                                            <div class="form-group">
                                                                <label>Image</label>
                                                                <input type="file" class="form-control image-file" name="picture" accept="image/*" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">

                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Page end  -->
    </div>

    <!-- Category Form Modal Start-->

    <!-- category Form Modal End-->

    <!-- Modal Edit -->
</div>
</div>

<!-- Wrapper End-->
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/footer.php'); ?>