<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/config.php');

use Galaxy\Database;

$category = new Database();

$data_list = $category->showData('categories', 5);

// dd($data_list);
?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/header.php'); ?>
<!-- include file sidebar -->
<div class="iq-sidebar sidebar-default" id="sidebar">
  <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/sidebar.php'); ?>
</div>
<!-- include file topnavbar -->
<div class="iq-top-navbar" id="topbar">
  <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/topbar.php'); ?>
</div>

<div class="content-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="d-flex flex-wrap align-items-center justify-content-between mb-4">
          <div>
            <h4 class="mb-3">Category List</h4>
          </div>
          <button data-toggle="modal" data-target="#categoryForm" class="btn btn-primary add-list">
            <i class="las la-plus mr-3"></i>Add Category
          </button>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="table-responsive rounded mb-3">
          <table class="data-table table mb-0 tbl-server-info">
            <thead class="bg-white text-uppercase">
              <tr class="ligth ligth-data">
                <th>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox1" />
                    <label for="checkbox1" class="mb-0"></label>
                  </div>
                </th>
                <th>Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody class="ligth-body">
              <?php if (!empty($data_list)) {
                foreach ($data_list as $data) :
              ?>
                  <tr>
                    <td>
                      <div class="checkbox d-inline-block">
                        <input type="checkbox" class="checkbox-input" id="checkbox2" />
                        <label for="checkbox2" class="mb-0"></label>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <img src="<?php echo $webroot . '/storage/uploads/' . $data['image_url']; ?>" class="img-fluid rounded avatar-50 mr-3" alt="image" />

                      </div>
                    </td>
                    <td><?= $data['title']; ?></td>
                    <td><?= $data['description']; ?></td>
                    <td>
                      <div class="d-flex align-items-center list-action">
                        <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                        <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="edit.php?id=<?= $data['id']; ?>"><i class="ri-pencil-line mr-0"></i></a>
                        <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" onclick="confirm('Are you sure?')" data-original-title="Delete" href="delete.php?id=<?= $data['id']; ?>"><i class="ri-delete-bin-line mr-0"></i></a>
                      </div>
                    </td>
                  </tr>
                <?php endforeach;
              } else { ?>
                <tr>
                  <td>Data not found!</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- Page end  -->
  </div>

  <!-- Category Form Modal Start-->
  <div class="modal fade" id="categoryForm" tabindex="-1" role="dialog" aria-labelledby="categoryForm" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form action="store.php" method="POST" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="categoryForm">Category Add</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">

                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Category Name *</label>
                            <input type="text" class="form-control" name="title" placeholder="Enter category Name" required />
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Description </label>
                            <textarea type="text" class="form-control" name="description" placeholder="Enter Code"></textarea>
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Image</label>
                            <input type="file" class="form-control image-file" name="picture" accept="image/*" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- category Form Modal End-->

  <!-- Modal Edit -->
</div>
</div>
<!-- Wrapper End-->
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/Backend/includes/footer.php'); ?>