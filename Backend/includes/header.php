<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>IMS Dash | Admin Dashboard</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/Inventory-PHP/Backend/assets/images/favicon.ico" />
    <link rel="stylesheet" href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/Inventory-PHP/Backend/assets/css/backend-plugin.min.css" />
    <link rel="stylesheet" href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/Inventory-PHP/Backend/assets/css/backend.css?v=1.0.0" />
    <link rel="stylesheet" href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/Inventory-PHP/Backend/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" />
    <link rel="stylesheet" href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/Inventory-PHP/Backend/assets/vendor/line-awesome/dist/line-awesome/css/line-awesome.min.css" />
    <link rel="stylesheet" href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/Inventory-PHP/Backend/assets/vendor/remixicon/fonts/remixicon.css" />
</head>

<body class="  ">
    <!-- loader Start -->
    <div id="loading">
        <div id="loading-center"></div>
    </div>
    <!-- loader END -->

    <!-- Wrapper Start -->
    <div class="wrapper">
        <!-- ============================================================ -->