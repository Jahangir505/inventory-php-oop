<?php include_once('./includes/header.php'); ?>
<!-- include file sidebar -->
<div class="iq-sidebar sidebar-default" id="sidebar">
  <?php include_once './includes/sidebar.php'; ?>
</div>
<!-- include file topnavbar -->
<div class="iq-top-navbar" id="topbar">
  <?php include_once('./includes/topbar.php'); ?>
</div>
<div class="content-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="d-flex flex-wrap align-items-center justify-content-between mb-4">
          <div>
            <h4 class="mb-3">Category List</h4>
          </div>
          <button data-toggle="modal" data-target="#categoryForm" class="btn btn-primary add-list">
            <i class="las la-plus mr-3"></i>Add Category
          </button>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="table-responsive rounded mb-3">
          <table class="data-table table mb-0 tbl-server-info">
            <thead class="bg-white text-uppercase">
              <tr class="ligth ligth-data">
                <th>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox1" />
                    <label for="checkbox1" class="mb-0"></label>
                  </div>
                </th>
                <th>Image</th>
                <th>Code</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody class="ligth-body">
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox2" />
                    <label for="checkbox2" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/01.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Organic Cream
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>CREM01</td>
                <td>Beauty</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox3" />
                    <label for="checkbox3" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/02.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Rain Umbrella
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>UM01</td>
                <td>Grocery</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox4" />
                    <label for="checkbox4" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/03.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Serum Bottle
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>SEM01</td>
                <td>Beauty</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox5" />
                    <label for="checkbox5" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/04.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Coffee Beans
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>COF01</td>
                <td>Food</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox6" />
                    <label for="checkbox6" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/05.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Book Shelves
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>FUN01</td>
                <td>Furniture</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox7" />
                    <label for="checkbox7" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/06.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Dinner Set
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>DIS01</td>
                <td>Grocery</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox8" />
                    <label for="checkbox8" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/07.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Nike Shoes
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>NIS01</td>
                <td>Shoes</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox9" />
                    <label for="checkbox9" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/08.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Computer Glasses
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>COG01</td>
                <td>Frames</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="checkbox d-inline-block">
                    <input type="checkbox" class="checkbox-input" id="checkbox10" />
                    <label for="checkbox10" class="mb-0"></label>
                  </div>
                </td>
                <td>
                  <div class="d-flex align-items-center">
                    <img src="./assets/images/table/product/09.jpg" class="img-fluid rounded avatar-50 mr-3" alt="image" />
                    <div>
                      Alloy Jewel Set
                      <p class="mb-0">
                        <small>This is test Product</small>
                      </p>
                    </div>
                  </div>
                </td>
                <td>AJS01</td>
                <td>Jewellery</td>
                <td>
                  <div class="d-flex align-items-center list-action">
                    <a class="badge badge-info mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="View" href="#"><i class="ri-eye-line mr-0"></i></a>
                    <a class="badge bg-success mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line mr-0"></i></a>
                    <a class="badge bg-warning mr-2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line mr-0"></i></a>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- Page end  -->
  </div>

  <!-- Category Form Modal Start-->
  <div class="modal fade" id="categoryForm" tabindex="-1" role="dialog" aria-labelledby="categoryForm" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form action="category.html">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="categoryForm">Category Add</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Image</label>
                            <input type="file" class="form-control image-file" name="pic" accept="image/*" />
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label> Name *</label>
                            <input type="text" class="form-control" placeholder="Enter Product Name" required />
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                            <label>Code *</label>
                            <input type="text" class="form-control" placeholder="Enter Code" required />
                            <div class="help-block with-errors"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- Category Form Modal End-->
</div>
</div>
<?php include_once('./includes/footer.php'); ?>