<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/config.php');

use Galaxy\Brands\Brand;
use Galaxy\Database;

$brand = new Brand();
$database = new Database();

$id = isset($_POST['id']) ? $_POST['id'] : "";
$title = isset($_POST['title']) ? $_POST['title'] : "";
$description = isset($_POST['description']) ? $_POST['description'] : "";


if (!empty($_FILES['picture']['name'])) {


    $image_name = $_FILES['picture']['name'];
    $target = $_FILES['picture']['tmp_name'];
    // $uniq_name = uniqid(rand(), true) . $image_name;

    $destination = $upload_uri . "/" . $image_name;


    $is_file_uploaded = move_uploaded_file($target, $destination);
} else {
    $image_name = isset($_POST['picture_old']) ? $_POST['picture_old'] : "";
}

if ($id > 0) {
    $data = ['id' => $id, 'title' => $title, 'description' => $description, 'image_url' => $image_name];
} else {
    $data = ['title' => $title, 'description' => $description, 'image_url' => $image_name];
}

// print_r($data);
// die;


$database->brandStoreOrUpdate($id, $data);
