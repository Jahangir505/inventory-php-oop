<?php

include_once($_SERVER['DOCUMENT_ROOT'] . '/Inventory-PHP/config.php');

use Galaxy\Brands\Brand;
use Galaxy\Database;

$brand = new Brand();
$database = new Database();

$id = isset($_GET['id']) ? $_GET['id'] : "";

$result = $database->deleteData($id, "brands");


if ($result) {
    echo "Data Delete Successfully!";
    r("http://php-oop.test/Inventory-PHP/backend/brands/brand.php");
} else {
    echo "Data Delete Faild!";
}
